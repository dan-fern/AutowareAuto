Design {#design}
======

This section outlines the AutowareAuto design principles, and links to relevant design documents.

- @subpage autoware-fusion-design
