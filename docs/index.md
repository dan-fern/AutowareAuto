Autoware.Auto {#index}
======================

# Installation and development

Install AutowareAuto and learn how to develop applications.

- @subpage installation-and-development


# Links to internal resources

- @subpage howto
- @subpage tutorials
- @subpage design "Design documents"
- @subpage coverage "Coverage reports"
